import pygame
import sys

# Initialize Pygame
pygame.init()

# Screen dimensions
SCREEN_WIDTH, SCREEN_HEIGHT = 800, 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

# Colors
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
GREEN = (0, 255, 0)
YELLOW = (255, 255, 0)
GREY = (128, 128, 128)

# Fonts
font = pygame.font.Font(None, 36)

# Game states
STATE_INSTRUCTIONS = 0
STATE_PUZZLE = 1
STATE_TEXT_SCREEN = 2
STATE_FINAL_SCREEN = 3

game_state = STATE_INSTRUCTIONS
current_puzzle = 0

# Puzzle words and current guess
puzzle_words = ["hello", "bird", "dog", "mouse", "tree"]
current_guess = ""
attempts = []
max_attempts = 6

# Hints for each puzzle word
puzzle_hints = [
    "A greeting",  # Hint for "hello"
    "A common flying animal",  # Hint for "bird"
    "A four-legged friend",  # Hint for "dog"
    "A small, furry creature",  # Hint for "mouse"
    "A part of nature, tall and green",  # Hint for "tree"
]

# Load final image (ensure you have an image named 'robot.png')
try:
    final_image = pygame.image.load('robot.png').convert()
except pygame.error:
    final_image = pygame.Surface((400, 300))  # Placeholder if image load fails
    final_image.fill(GREY)

clock = pygame.time.Clock()

# Main game loop
running = True
while running:
    screen.fill(WHITE)
    
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        if event.type == pygame.KEYDOWN:
            if game_state == STATE_INSTRUCTIONS and event.key == pygame.K_SPACE:
                game_state = STATE_PUZZLE
            elif game_state == STATE_PUZZLE:
                if event.key == pygame.K_RETURN and len(current_guess) == len(puzzle_words[current_puzzle]):
                    attempts.append(current_guess)
                    if current_guess.lower() == puzzle_words[current_puzzle]:
                        if current_puzzle == len(puzzle_words) - 1:
                            game_state = STATE_FINAL_SCREEN
                        else:
                            game_state = STATE_TEXT_SCREEN
                        current_guess = ""
                        attempts.clear()
                    elif len(attempts) >= max_attempts:
                        current_puzzle += 1
                        if current_puzzle >= len(puzzle_words):
                            game_state = STATE_FINAL_SCREEN
                        else:
                            game_state = STATE_TEXT_SCREEN
                        current_guess = ""
                        attempts.clear()
                elif event.key == pygame.K_BACKSPACE:
                    current_guess = current_guess[:-1]
                elif len(current_guess) < len(puzzle_words[current_puzzle]) and event.unicode.isalpha():
                    current_guess += event.unicode.lower()
            elif game_state == STATE_TEXT_SCREEN and event.key == pygame.K_SPACE:
                current_puzzle += 1
                if current_puzzle >= len(puzzle_words):
                    game_state = STATE_FINAL_SCREEN
                else:
                    game_state = STATE_PUZZLE
                    current_guess = ""
                    attempts = []

    if game_state == STATE_INSTRUCTIONS:
        instructions_text = font.render("Press Space to start", True, BLACK)
        screen.blit(instructions_text, (100, 100))
    elif game_state == STATE_PUZZLE:
        # Display the hint for the current puzzle
        hint_text = font.render("Hint: " + puzzle_hints[current_puzzle], True, BLACK)
        screen.blit(hint_text, (100, 150))
        
        # Display current guess and placeholders for letters
        for i in range(len(puzzle_words[current_puzzle])):
            letter = current_guess[i] if i < len(current_guess) else "_"
            letter_text = font.render(letter.upper(), True, BLACK)
            screen.blit(letter_text, (100 + i * 50, 200))
        # Display attempts
        for i, attempt in enumerate(attempts):
            for j, letter in enumerate(attempt):
                color = GREY
                if letter == puzzle_words[current_puzzle][j]:
                    color = GREEN
                elif letter in puzzle_words[current_puzzle]:
                    color = YELLOW
                letter_text = font.render(letter.upper(), True, color)
                screen.blit(letter_text, (100 + j * 50, 250 + i * 50))
    elif game_state == STATE_TEXT_SCREEN:
        text_screen_text = font.render(f"Puzzle {current_puzzle + 1} Solved!", True, BLACK)
        screen.blit(text_screen_text, (100, 100))
    elif game_state == STATE_FINAL_SCREEN:
        final_text = font.render("Congratulations, you've completed the game!", True, BLACK)
        screen.blit(final_text, (100, 100))
        screen.blit(final_image, (200, 150))

    pygame.display.flip()
    clock.tick(60)

pygame.quit()
sys.exit()  # Ensure to properly exit the program
