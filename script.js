const cards = document.querySelectorAll(".card");

let matched = 0;
let cardOne, cardTwo;
let disableDeck = false;

// function flipCard({target: clickedCard}) {
//     if(clickedCard !== cardOne && !disableDeck) {
//         clickedCard.classList.add("flip");
//         if(!cardOne) {
//             return cardOne = clickedCard;
//         }
//         cardTwo = clickedCard;
//         disableDeck = true;
//         let cardOneImg = cardOne.querySelector(".back-view img").src,
//             cardTwoImg = cardTwo.querySelector(".back-view img").src;
//         matchCards(cardOneImg, cardTwoImg);
//     }
// }




// Close modal when clicking anywhere outside of the modal content
window.onclick = function(event) {
    // Again, make sure the ID matches the one used in the HTML
    if (event.target == document.getElementById("videoModal")) {
        document.getElementById("videoModal").style.display = "none";
    }
};

if(matched == 1) { // Assuming you want to show the video after the fourth match
    setTimeout(() => {
        showModal('https://srt32.gitlab.io/resume-game/videos/video-1.mp4'); // Use the correct URL here
    }, 1000);
}

if(matched == 2) { // Assuming you want to show the video after the fourth match
    setTimeout(() => {
        showModal('https://srt32.gitlab.io/resume-game/videos/video-2.mp4'); // Use the correct URL here
    }, 1000);
}

if(matched == 3) { // Assuming you want to show the video after the fourth match
    setTimeout(() => {
        showModal('https://srt32.gitlab.io/resume-game/videos/video-3.mp4'); // Use the correct URL here
    }, 1000);
}

if(matched == 4) { // Assuming you want to show the video after the fourth match
    setTimeout(() => {
        showModal('https://srt32.gitlab.io/resume-game/videos/video-1.mp4'); // Use the correct URL here
    }, 1000);
}

// function matchCards(img1, img2) {
//     if (img1 === img2) {
//         matched++; // Increment the number of matches
//         // Play a video for the match; you might need to adjust the naming of your videos to match this pattern
//         showModal(`https://srt32.gitlab.io/resume-game/videos/video-${matched}.mp4`);
//         cardOne.removeEventListener("click", flipCard);
//         cardTwo.removeEventListener("click", flipCard);
//     } else {
//         setTimeout(() => {
//             cardOne.classList.remove("flip");
//             cardTwo.classList.remove("flip");
//         }, 1200);
//     }
//     resetCards(); // Reset cards whether it's a match or not
// }


// function matchCards(img1, img2) {
//     if (img1 === img2) {
//         matched++;
//         showModal(`https://srt32.gitlab.io/resume-game/videos/video-${matched}.mp4`);
//         cardOne.removeEventListener("click", flipCard);
//         cardTwo.removeEventListener("click", flipCard);
//         resetCards(); // Reset cards after a successful match
//     } else {
//         setTimeout(() => {
//             // Add the checks here
//             if (cardOne) cardOne.classList.remove("flip");
//             if (cardTwo) cardTwo.classList.remove("flip");
//             resetCards(); // Reset cards after an unsuccessful match
//         }, 1200);
//     }
// }

// function matchCards(img1, img2) {
//     if(img1 === img2) {
//         matched++;
        
//         if(matched == 4) { // Since there are only 4 pairs
//             setTimeout(() => {
//                 showModal(`videos/video-${matched}.mp4`); // Call showModal with the video source
//             }, 1000);
        
//         cardOne.removeEventListener("click", flipCard);
//         cardTwo.removeEventListener("click", flipCard);
//         resetCards();
//     } else {
//         setTimeout(() => {
//             cardOne.classList.add("shake");
//             cardTwo.classList.add("shake");
//         }, 400);

//         setTimeout(() => {
//             cardOne.classList.remove("shake", "flip");
//             cardTwo.classList.remove("shake", "flip");
//             resetCards();
//         }, 1200);
//     }
// }

// function resetCards() {
//     cardOne = cardTwo = "";
//     disableDeck = false;
// }

// function resetCards() {
//     // Reset card variables after the timeout so they're not undefined
//     cardOne = null;
//     cardTwo = null;
//     disableDeck = false;
// }

function flipCard({target: clickedCard}) {
    if(clickedCard !== cardOne && !disableDeck) {
        clickedCard.classList.add("flip");
        if(!cardOne) {
            cardOne = clickedCard;
        } else {
            cardTwo = clickedCard;
            disableDeck = true;
            let cardOneImg = cardOne.querySelector(".back-view img").src,
                cardTwoImg = cardTwo.querySelector(".back-view img").src;
            matchCards(cardOneImg, cardTwoImg);
        }
    }
}

function matchCards(img1, img2) {
    if(img1 === img2) {
        matched++;
        showModal(`https://srt32.gitlab.io/resume-game/videos/video-${matched}.mp4`);
        cardOne.removeEventListener("click", flipCard);
        cardTwo.removeEventListener("click", flipCard);
        resetCards(); // Reset cards after a successful match
    } else {
        // We are already inside a closure, so we don't need to check if cardOne and cardTwo exist
        setTimeout(() => {
            cardOne.classList.remove("flip");
            cardTwo.classList.remove("flip");
            resetCards(); // Reset cards after an unsuccessful match
        }, 600);
    }
}

function resetCards() {
    // Delay the resetting of the cards to allow the flip transition to complete
    setTimeout(() => {
        cardOne = null;
        cardTwo = null;
        disableDeck = false;
    }, 600);
}



function shuffleCard() {
    matched = 0;
    cards.forEach(card => {
        card.classList.remove("flip");
        const randomPos = Math.floor(Math.random() * 8); // 8 because there are 4 pairs x 2
        card.style.order = randomPos;
        card.addEventListener("click", flipCard);
    });
}

shuffleCard(); // Call shuffleCard when the game is first loaded

// Modal functionality
const modal = document.getElementById("showModal");
const modalContent = document.querySelector(".modal-content");

// function showModal(videoSrc) {
//     const videoHtml = `
//         <video width="320" height="240" controls autoplay>
//             <source src="${videoSrc}" type="video/mp4">
//             Your browser does not support the video tag.
//         </video>
//     `;
//     // Make sure this targets the correct modal content container
//     modalContent.innerHTML = videoHtml;
//     // Make sure the modal ID matches the one in the HTML
//     document.getElementById("videoModal").style.display = "block";
// };

// function showModal(videoSrc) {
//     const videoPlayer = document.getElementById("videoPlayer");
//     const sourceElement = videoPlayer.querySelector("source");

//     sourceElement.src = videoSrc; // Set the new source for the video
//     videoPlayer.load(); // Load the new video source
//     document.getElementById("videoModal").style.display = "block";
// }

// function showModal(videoSrc) {
//     const videoPlayer = document.getElementById("videoPlayer");
//     videoPlayer.src = videoSrc;
//     videoPlayer.load(); // Important: call load to update the video source
//     document.getElementById("videoModal").style.display = "block";
// }


// function showModal(videoSrc) {
//     const videoPlayer = document.getElementById("videoPlayer");
//     if (videoPlayer) {
//         const sourceElement = videoPlayer.querySelector("source");
//         sourceElement.src = videoSrc; // Set the new source for the video
//         videoPlayer.load(); // Load the new video source
//         videoPlayer.play().catch(e => console.error("Error trying to play video:", e)); // Try to play the video
//         document.getElementById("videoModal").style.display = "block";
//     } else {
//         console.error("Video player element not found!");
//     }
// }

// function showModal(videoSrc) {
//     console.log('Attempting to show video:', videoSrc);
//     const videoModal = document.getElementById("videoModal");
//     const videoPlayer = videoModal.querySelector("#videoPlayer");
//     const sourceElement = videoPlayer.querySelector("source");

//     sourceElement.src = videoSrc; // Update the source of the video
//     videoPlayer.load(); // Load the new video source
//     videoPlayer.play().catch(e => console.error("Error trying to play video:", e)); // Try to play the video
//     videoModal.style.display = "block"; // Show the modal
// }

function showModal(videoSrc) {
    const videoModal = document.getElementById("videoModal");
    const videoPlayer = videoModal.querySelector("#videoPlayer");
    const sourceElement = videoPlayer.querySelector("source");

    sourceElement.src = videoSrc; // Update the source of the video
    videoPlayer.load(); // Load the new video source
    videoModal.style.display = "block"; // Show the modal

    videoPlayer.play().catch(e => {
        // Handle the promise rejection if the browser doesn't allow autoplay
        console.error("Error trying to play video:", e);
        // You might want to display controls for the user to manually play the video
        videoPlayer.controls = true;
    });
}
